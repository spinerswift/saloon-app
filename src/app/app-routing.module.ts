import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SidenavComponent } from './sidenav/sidenav.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { PeriodicReportsComponent } from './periodic-reports/periodic-reports.component';
import { EmployeesComponent } from './employees/employees.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingsComponent } from './settings/settings.component';
import { VisitreportComponent } from './visitreport/visitreport.component';
import { EmployeereportComponent } from './employeereport/employeereport.component';
import { SuperloginComponent } from './superlogin/superlogin.component';
import { SupersidenavComponent } from './supersidenav/supersidenav.component';
import { SuperdashboardComponent } from './superdashboard/superdashboard.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';


const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'umukozi', component: EmployeereportComponent},
  {path: 'super-login', component: SuperloginComponent},
  {path: 'dashboard', component: SidenavComponent, children: [
    {path: '', component: DashboardComponent},
    {path: 'periodic-reports', component: PeriodicReportsComponent},
    {path: 'profile', component: ProfileComponent},
    {path: 'settings', component: SettingsComponent},
    {path: 'records', component: VisitreportComponent},
    {path: 'subscriptions', component: SubscriptionsComponent},
    // {path: 'add-employee', component: AddEmployeeComponent},
    {path: 'employees', component: EmployeesComponent},
    
    
]},
{path: 'super-dashboard', component: SupersidenavComponent, children: [
  {path: '', component: SuperdashboardComponent},

  
]},
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
