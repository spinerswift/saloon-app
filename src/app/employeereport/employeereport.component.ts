import { Component, OnInit } from '@angular/core';
import {formatDate} from '@angular/common';
import { ApiService } from '../api.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-employeereport',
  templateUrl: './employeereport.component.html',
  styleUrls: ['./employeereport.component.scss']
})
export class EmployeereportComponent implements OnInit {
profile: any;
date = new Date();
report: any;
error: any;
nid: any;
day: any;
error_msg: any;
employeeForm: FormGroup;
  constructor(private api: ApiService, private fb: FormBuilder) { }

  ngOnInit(): void {
    //this.day = "2020-07-11"
    this.day = formatDate(this.date, 'yyyy-MM-dd', 'en-US');
    this.employeeForm = this.fb.group({
      
      nid:  [this.nid ? this.nid:'', [Validators.required]],
     
    });

  }
  getReport(data){
    this.error = null;
      this.report = null;
      let response = this.api.getEmployeeReport(data,this.day);
     
      response.subscribe((response: any)=> {
          
            this.report = response;
            this.nid = this.report.nid
            this.error_msg = null
            this.ngOnInit()
            this.api.setLoadingStatus(false);
            
        },
        error => {
          if (error.status == 400) {
            this.api.setLoadingStatus(false);
            this.error_msg = "Andika neza nimero y'indangamuntu"
          }
          else if (error.status == 0) {
            this.api.setLoadingStatus(false);
            this.error_msg = "Nta internet ufite"
          }
         
        }
      );
    }

}
