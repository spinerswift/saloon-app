import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodicReportsComponent } from './periodic-reports.component';

describe('PeriodicReportsComponent', () => {
  let component: PeriodicReportsComponent;
  let fixture: ComponentFixture<PeriodicReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodicReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodicReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
