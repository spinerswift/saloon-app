import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-superdashboard',
  templateUrl: './superdashboard.component.html',
  styleUrls: ['./superdashboard.component.scss']
})
export class SuperdashboardComponent implements OnInit {
  periodForm: FormGroup;
  error_msg: any;
  report: any;
  error: any;
  isLoading: any;
  report_message: any;
  dates: any;

  constructor(private api: ApiService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.api.isLoading.subscribe((status: boolean) => {
      this.isLoading = status;
    });
    this.api.setLoadingStatus(false);
    
    
      this.periodForm = this.fb.group({
        startDate: ['', [Validators.required]],
        endDate: ['', [Validators.required]]
      });

}

getReport(data){
  this.error = null;
  this.dates = data
    this.report = null;
    let response = this.api.getPeriodicSaloonsReport(data);
    response.subscribe((response: any)=> {
        
          this.report = response;
          this.report_message = null
          console.log(this.report)
          
          this.api.setLoadingStatus(false);
         
        
      },
      error => {
        this.error = error;
        this.api.setLoadingStatus(false);
        if (error.status == 400) {
         
          this.report_message = "No Records"
        }
        else if (error.status == 500) {
         
          this.report_message = "Enter start and end dates" 
        }
      }
    );
  }
  print(){
    window.print()
  }

  permission(saloon, permission){
    this.error = null;
     
      let response = this.api.updatePermission(saloon, permission);
      response.subscribe((response: any)=> {
        this.api.setLoadingStatus(false);
        this.getReport(this.dates)
       
          
        },
        error => {
          this.error = error;
          this.api.setLoadingStatus(false);
          if (error.status == 400) {
           
            this.report_message = "No Saloon"
          }
          else if (error.status == 0) {
           
            this.report_message = "No internet connection" 
          }
        }
      );
    }
}
